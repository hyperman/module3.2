<?php

class File
{

    private $file_path;
    private $file_name;



    public function __construct($file_path)
    {


        if (!file_exists($file_path)) {

            throw new Exception("Error, File Not Exists!");   //first
        } else {
            $this->file_path = $file_path;
        }


    }

    public function file_name()
    {

        $name = basename($this->file_path);
        $this->file_name =  $name;
        echo 'File:'.$name." | ";

    }

    public function file_size(){
        $size = filesize($this->file_path);
        $size =  $size / 1024;

        echo 'size:'.$size." kB | ";

    }

    public function read_write() {
        echo (is_readable($this->file_path)? "is " : "is not "). "readable | ";

        if(is_writable( $this->file_name)) {
            echo "is writable | ";
        } else {
            echo "is not writable | ";
        }

    }

    public function creating_data() {

        $file_create = date('d F Y H:i:s.', filectime($this->file_path));
        echo "created: ". $file_create." | ";

    }

    public function last_moderation_date() {
        $file_mod = $file_open = date('d F Y H:i:s.', filemtime($this->file_path));
        echo "last opened: ". $file_mod." | ";
        $file_open = date('d F Y H:i:s.PHP_EOL.', fileatime($this->file_path));
        echo "last modify: ". $file_open;



    }



}