<?php

class File
{

    private $file_path;

    public function __construct($file_path)
    {


        if (!file_exists($file_path)) {
            touch($file_path);
            $this->file_path = $file_path;

        }
        $this->file_path = $file_path;

    }
    public function file_name()
    {

        $name = basename($this->file_path);
        $this->file_name =  $name;
        echo 'File:'.$name." | ";

    }

    public function file_size(){
        $size = filesize($this->file_path);
        $size =  $size / 1024;

        echo 'size:'.$size." kB | ";

    }

    public function read_write() {
        echo (is_readable($this->file_path)? "is " : "is not "). "readable | ";

        if(is_writable( $this->file_name)) {
            echo "is writable | ";
        } else {
            echo "is not writable | ";
        }

    }

    public function creating_data() {

        $file_create = date('d F Y H:i:s.', filectime($this->file_path));
        echo "created: ". $file_create." | ";

    }

    public function last_moderation_date() {
        $file_mod = $file_open = date('d F Y H:i:s.', filemtime($this->file_path));
        echo "last opened: ". $file_mod." | ";
        $file_open = date('d F Y H:i:s.PHP_EOL', fileatime($this->file_path));
        echo "last modify: ". $file_open;



    }

    public function get_content() {


        $handler = fopen( $this->file_path, 'r');

        $content = '';
        while(!feof($handler)) {
            $content .= fgets($handler);
        }

        fclose($handler);
        return $content;
    }


    public function put_content() {
        $handler  = fopen($this->file_path, 'w');
        for($i =0; $i <50; $i++)  {
            fwrite( $handler, Strings::get_random_string(100).PHP_EOL);
        }
        fclose($handler);
    }

    public function put_dop_content_() {
        $handler  = fopen($this->file_path, 'a');
        fwrite($handler, "111111111111111111111111111111111111111111111111111".PHP_EOL);
        fclose($handler);
    }


}














